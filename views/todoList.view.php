<h1><?php echo $h1 ?></h1>
<h2>List</h2>
<a href="http://localhost/todo/add">Add a todo</a>
<table>
    <tr>
        <th>id</th>
        <th>Description</th>
        <th>is Done</th>
        <th>created At</th>
        <th>actions</th>
    </tr>
    <?php foreach($todos as $todo): ?>
        <tr>
            <td><?php echo $todo['id'] ?></td>
            <td><?php echo $todo['description'] ?></td>
            <td><input type="checkbox" <?php if($todo['isDone']) echo 'checked' ?> disabled></td>
            <td><?php echo date('Y-M-d h:i', strtotime($todo['createdAt'])) ?></td>
            <td><a href="http://localhost/todo/update?id=<?php echo $todo['id'] ?>">Update</a> | <a href="http://localhost/todo/delete?id=<?php echo $todo['id'] ?>">Delete</a></td>
        </tr>
    <?php endforeach ?>
</table>
