<form action="<?php print_r('http://localhost/' . $config['struct']['action']); ?>"
      method="<?php print_r($config['struct']['method']); ?>">
    <?php foreach ($config['data'] as $name => $attributs): ?>

        <?php if (isset($attributs['option']) && $attributs['option'] == 'repeated'): ?>
            <label>
                <?php echo isset($attributs['label']) ?>

                <input type="<?php echo $attributs['type'] ?>"
                       name="<?php echo $name . '_first' ?>"
                       placeholder="<?php echo $attributs['placeholder'] ?>"
                    <?php echo (isset($attributs["required"])) ? "required" : ""; ?>
                >
                <input type="<?php echo $attributs['type'] ?>"
                       name="<?php echo $name . '_second' ?>"
                       placeholder="<?php echo $attributs['second_placeholder'] ?>"
                    <?php echo (isset($attributs["required"])) ? "required" : ""; ?>
                >
            </label>
        <?php endif; ?>

        <?php if ($attributs['type'] == 'text' or $attributs['type'] == 'email' or $attributs['type'] == 'date' or ($attributs['type'] == 'password') && isset($attributs['option']) != 'repeated'): ?>
            <input type="<?php echo $attributs['type'] ?>"
                   name="<?php echo $name ?>"
                   <?php if(isset($attributs['value'])): ?>
                   value="<?php echo $attributs['value'] ?>"
                   <?php endif;?>
                    <?php if(isset($attributs['hidden'])): ?>
                    hidden="hidden"
                    <?php endif; ?>
                   <?php if(isset($attributs['placeholder'])): ?>
                   placeholder="<?php echo $attributs['placeholder'] ?>"
                   <?php endif; ?>
                <?php echo (isset($attributs["required"])) ? "required" : ""; ?>>
        <?php endif; ?>

        <?php if ($attributs['type'] == 'longtext'): ?>
            <textarea name="<?php echo $name ?>" placeholder="<?php echo $attributs['placeholder'] ?>" class="wysiwyg" cols="30" rows="10">
                <?php if (isset($attributs['value'])): ?>
                    <?php echo $attributs['value']; ?>
                <?php endif; ?>
            </textarea>
        <?php endif; ?>

        <?php if ($attributs['type'] == 'textarea'): ?>
            <textarea name="<?php echo $name ?>" placeholder="<?php echo $attributs['placeholder'] ?>" cols="30" rows="10">
                <?php if (isset($attributs['value'])): ?>
                    <?php echo $attributs['value']; ?>
                <?php endif; ?>
            </textarea>
        <?php endif; ?>

        <?php if ($attributs['type'] == 'checkbox'): ?>
            <label>
                <input type="<?php echo $attributs['type'] ?>"
                       name="<?php echo $name ?>"
                       <?php if(isset($attributs['value']) && $attributs['value'] == 1): ?>
                            checked="checked"
                       <?php endif; ?>
                    <?php echo (isset($attributs["required"])) ? "required" : ""; ?>>
                <?php echo $attributs['label'] ?>
            </label>
        <?php endif; ?>

        <?php if ($attributs['type'] == 'select'): ?>
            <label>
                <select name="<?php echo $name; ?>" <?php echo (isset($attributs["required"])) ? "required" : ""; ?>>
                    <?php if (isset($attributs['placeholder'])): ?>
                        <option disabled><?php echo $attributs['placeholder']; ?></option>
                    <?php endif; ?>

                    <?php foreach($attributs['rules']['choices'] as $key => $value): ?>
                        <?php if(isset($attributs['value']) && $attributs['value'] == $value): ?>
                            <option value="<?php echo $value; ?>" selected><?php echo $key; ?></option>
                        <?php else: ?>
                            <option value="<?php echo $value; ?>"><?php echo $key; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </label>
        <?php endif; ?>
    <?php endforeach; ?>
    <button type="submit"><?php print_r($config['struct']['submit']); ?></button>
</form>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'textarea.wysiwyg',
        resize: false
    });
</script>