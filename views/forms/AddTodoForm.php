<?php

class AddTodoForm
{
    public function buildView()
    {
        return [
            'description' => [
                'type' => 'text',
                'placeholder' => 'To do',
                'required' => true
            ],
            'isDone' => [
                'type' => 'checkbox',
                'label' => 'Is the task done?'
            ]
        ];
    }

    public function configureOptions()
    {
        return [
            'method' => 'POST',
            'action' => 'todo/adding',
            'submit' => 'Add to do'
        ];
    }
}