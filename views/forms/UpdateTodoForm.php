<?php

class UpdateTodoForm
{
    public function buildView()
    {
        return [
            'description' => [
                'type' => 'text',
                'placeholder' => 'To do',
                'required' => true
            ],
            'isDone' => [
                'type' => 'checkbox',
                'label' => 'Is the task done?'
            ]
        ];
    }

    public function configureOptions()
    {
        return [
            'method' => 'POST',
            'action' => 'todo/updating',
            'submit' => 'Update to do'
        ];
    }
}