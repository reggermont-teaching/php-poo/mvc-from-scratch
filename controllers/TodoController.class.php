<?php

Class TodoController 
{
    public function defaultAction()
    {
        $this->listAction();
    }

    public function listAction()
    {
        $todo = new Todo();
        $todos = $todo->getAllBy();

        // Exercice 1
        usort ($todos, function($left, $right) {
            return $left['isDone'] - $right['isDone'];
        });

        $view = new View('todoList');
        
        $view->fillData([
            'h1' => 'List of Todo',
            'todos' => $todos
        ]);
    }

    public function addAction()
    {
        $form = new Form(AddTodoForm::class);
        $view = new View('form');
        $view->fillData([
            'h1' => 'Add to do',
            'form' => $form->getForm()
        ]);
    }

    public function addingAction()
    {
        $todo = new Todo();
        $todo->populate(Request::getPostAll());
        $todo->setId(-1);
        $todo->save();
        Routing::redirect('todo');
    }

    public function updateAction()
    {
        $id = Request::get('id');

        if (!$id) {
            Routing::redirect('todo');
        } else {
            $_SESSION['id'] = $id;

            $todo = new Todo();
            $currentTodo = $todo->getOneBy(['id' => $id]);

            $form = new Form(UpdateTodoForm::class);
            $form->bindValue($currentTodo);
            $view = new View('form');
            $view->fillData([
                'h1' => 'Update to do',
                'form' => $form->getForm()
            ]);
        }

       
    }

    public function updatingAction()
    {
        $id = Request::getSession('id');

        if (!$id) {
            Routing::redirect('todo');
        } else {
            $todo = new Todo();
            $todo->populate(Request::getPostAll());
            $todo->setId($id);
            $todo->save();
            Routing::redirect('todo');
        }
    }

    public function deleteAction() {
        $id = Request::get('id');
        // http://localhost/todo/delete?id=3 ==> '3'

        if($id) {
            $todo = new Todo();
            $todo->delete($id);
        }

        Routing::redirect('todo');
    }    
}
