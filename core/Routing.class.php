<?php

Class Routing 
{
    private $uri;
    private $uriExploded;
    private $controller;
    private $controllerName;
    private $action;
    private $actionName;

    public function __construct()
    {
        $this->setUriExploded();
        $this->setController();
        $this->setAction();
        $this->runRoute();
    }

    private function setUriExploded() 
    {

        $this->uri = ltrim($_SERVER["REQUEST_URI"], '/');
        $this->uriExploded = explode("/",$this->uri);
        $this->uriExploded[1] = explode('?', $this->uriExploded[1])[0];
        // $this->uriExploded = delete?id=3
        // explode --> ['delete', 'id=3']
        // take first result
    }

    private function setController() 
    {
        $this->controller = empty($this->uriExploded[0])
            ? 'Index'
            : ucfirst($this->uriExploded[0]);
        $this->controllerName = $this->controller . 'Controller';
    }

    private function setAction() 
    {
        $this->action = empty($this->uriExploded[1])
            ? 'default'
            : $this->uriExploded[1];
        $this->actionName = $this->action . 'Action';
    }

    // http://localhost/todo/list
    // this->controller = Todo
    // this->controllerName = TodoController
    // this->action = List
    // this->actionName = ListAction

    public function checkRoute()
    {
        $pathController = '../controllers/' . $this->controllerName . '.class.php';
        return file_exists($pathController);
    }

    private function runRoute()
    {
        if($this->checkRoute()) {
            $controller = new $this->controllerName();
            $controller->{$this->actionName}();
        } else {
            $this->page404();
        }
    }

    public static function includeFormClass($formClass)
    {
        $path = '../views/forms/' . $formClass . '.php';

        if (file_exists($path)) {
            include $path;
        } else {
            die('Form Class doesn\'t exist');
        }
    }

    public function page404()
    {
        echo '404 - Page not Found';
    }

    public static function redirect($path = '') 
    {
        header("Location:http://localhost/" . $path);
    }

}