<?php

Class Form
{
    private $formClass;
    private $data;

    public function __construct($formClass)
    {
        if (!class_exists($formClass)) {
            Routing::includeFormClass($formClass);
        }

        $this->formClass = new $formClass;
        $this->data = $this->formClass->buildView();
    }

    public function getForm()
    {
        return [
            'data' => $this->data,
            'struct' => $this->formClass->configureOptions()
        ];
    }

    public function bindValue(array $values): void
    {
        // $values = ['id' => 4, 'description' => 'New todo', 'isDone' => false, 'createdAt' => '...']
        foreach($values as $key => $value) {
            if(array_key_exists($key, $this->getForm()['data'])) {
                $this->data[$key]['value'] = $value;
            }
        } 
    }
}