<?php

Class BaseSQL
{
    private $db;
    private $tableName;
    private $columns = [];

    // ['id' => 1, 'description' => 'Buy some food']

    public function __construct() {
        $this->db = BaseSQL::getDb();

        $modelName = strtolower(get_called_class()); // todo

        $this->tableName = $modelName[strlen($modelName) -1] == 'y'
            ? rtrim($modelName,'y') . 'ies'
            : $modelName . 's';

        $modelVars = get_class_vars(get_called_class());
        $baseSQLVars = get_class_vars(get_class());

        $this->columns = array_diff_key($modelVars, $baseSQLVars);
        unset($this->columns['createdAt']);
    }

    public function populate(array $values) {
        // $values = ['description' => 'Nouvelle tâche', 'isDone' => false]
        foreach($values as $key => $value) {
            // 
            $setter = 'set' . ucfirst($key); // setDescription

            if (method_exists(get_called_class(), $setter)) {
                $this->{$setter}($value); // $this->setDescription('Nouvelle tâche')
            }
        }
    }

    public function save(): bool {
        // INSERT
        if($this->id == -1) {

            unset($this->columns['id']);

            $data = [];
            $sqlColumns = '';
            $sqlKeys = '';

            // $this->columns = ['description', 'isDone']
            foreach ($this->columns as $column => $value) {
                $getter = 'get' . ucfirst($column);
                $data[$column] = $this->{$getter}();
                $sqlColumns .= ',"' . $column . '"';
                $sqlKeys .= ',:' . $column;
            }

            $sqlColumns = ltrim($sqlColumns, ',');
            $sqlKeys = ltrim($sqlKeys, ',');

            $query = $this->db->prepare(
                'INSERT INTO '
                . $this->tableName
                . '('
                . $sqlColumns
                . ') VALUES ('
                . $sqlKeys
                . ')'
            );

            $query->execute($data);


            // INSERT INTO todos(description,isDone) VALUES (:description,:isDone)

        } else {

            $data = [];
            $sqlColumnsAndKeys = [];

            foreach($this->columns as $column => $value) {
                $getter = 'get' . ucfirst($column);
                $data[$column] = $this->{$getter}();
                $sqlColumnsAndKeys[] = '"' . $column . '"=:' . $column;
            }
            // $sqlColumnsAndKeys = ['description=:description', 'isDone=:isDone']
            
            $query = $this->db->prepare(
                'UPDATE '
                . $this->tableName
                . ' SET '
                . implode(',', $sqlColumnsAndKeys)
                . ' WHERE "id"=:id'
            );

            $query->execute($data);

            // UPDATE todos SET "description"=:description,"isDone"=:isDone WHERE "id"=:id
        }

        return true;
    }

    public function getBy($conditions = [])
    {

        // $conditions = ['isDone' => 'false']

        $where = [];

        foreach ($conditions as $key => $value) {
            $where[] = '"' . $key . '"=:' . $key;
        }

        $sqlQuery = 'SELECT * FROM ' . $this->tableName;

        if (!empty($where)) {
            $sqlQuery .= ' WHERE ' . implode(',', $where);
        }

        $query = $this->db->prepare($sqlQuery);

        $query->execute($conditions);

        return $query->fetchAll(PDO::FETCH_ASSOC);
        // SELECT * FROM todos WHERE isDone=:isDone, ...
    }

    public function getAllBy($conditions = [])
    {
        return $this->getBy($conditions);
    }

    public function getOneBy($conditions)
    {
        return $this->getBy($conditions)[0];
    }

    public function delete(int $id): bool
    {
        $query = $this->db->prepare(
            'DELETE FROM '
            . $this->tableName
            . ' WHERE "id"=:id'
        );

        // DELETE FROM todos WHERE id=:id

        $query->execute(['id' => $id]);

        return true;
    }

    public static function getDb()
    {
        try {
            $dbText = DB_PROVIDER 
                . ':dbname='
                . DB_NAME
                . ';host='
                . DB_HOST
                . ';user='
                . DB_USER
                . ';password='
                . DB_PWD
                . ';port='
                . DB_PORT;
            
            $db = new PDO($dbText);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch(Exception $e) {
            die('SQL Error: ' . $e->getMessage());
        }
    }
}