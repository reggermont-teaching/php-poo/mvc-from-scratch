<?php

Class View 
{
    static $VIEW_PATH = '../views/';
    private $viewName;

    protected $data = [];

    public function __construct(string $view)
    {
        $this->setViewName($view);
    }

    public function setViewName($view)
    {
        $viewPath = View::$VIEW_PATH . $view . '.view.php';
        if (file_exists($viewPath)) {
            $this->viewName = View::$VIEW_PATH . $view . '.view.php';
        } else {
            die('Le fichier ' . $viewPath . ' n\'existe pas');
        }
    }

    public function includeForm($config)
    {
        include '../views/forms/form.view.php';
    }

    public function fillData(array $providedData = []): void
    {
        foreach ($providedData as $key => $value) {
            $this->data[$key]= $value;
        }
    }

    public function __destruct()
    {
        extract($this->data);
        include $this->viewName;

    }
}