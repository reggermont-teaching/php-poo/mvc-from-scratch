<?php

class Request
{
    public static function get($key): ?string
    {
        if(isset($_GET) && isset($_GET[$key]) && !empty($_GET[$key])) {
            return $_GET[$key];
        } else {
            return null;
        }
    }

    public static function getPostAll(): array
    {
        return isset($_POST) ? $_POST : [];
    }

    public static function getSession($key): ?string
    {
        if(isset($_SESSION) && isset($_SESSION[$key]) && !empty($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }
}