# PHP MVC From Scratch with CRUD Example (todo list)

## Exercises

### Exercise 1

#### Description

Postgres is default sorting by last updated. The goal of this exercise is to change the TodoController default sorting and display first the tasks not done, and then the tasks done

### Exercise 2

#### Description

Now that you sort the two types of todos, please separate them in two different divs with one h2 "To do" and the other "Already done"

### Exercise 3

#### Description

Add a config to set the URL base path and protocol dynamically. Use it for todo pages

Changes expected are for PHP Code, not PHP / Nginx config

#### Example

If my protocol is http and base path is localhost (like in your dev environment), so the path for the todo list would be `http://localhost/todo`  
For a production server, the protocol would be `https` and base path a real domain name (ex: `mywebsite.com`), so the path would be `https://mywebsite.com/todo`
