<?php

Class Todo extends BaseSQL
{
    // CREATE TABLE todos(id SERIAL PRIMARY KEY, description VARCHAR(200), isDone boolean, createdAt timestamp default current_timestamp)

    // INSERT INTO todos (description,isDone) VALUES ('First todo', false)

    protected $id;
    protected $description;
    protected $isDone;
    protected $createdAt;

    public function __construct()
    {
        parent::__construct();
    } 

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getIsDone(): ?string
    {
        return $this->isDone;
    }

    public function setIsDone(string $isDone): void
    {
        $this->isDone = $isDone == 'on' || $isDone == true || $isDone == 1;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

}